//
//  ChatMessageModel.swift
//  ChatDemo
//
//  Created by Ахмед Фокичев on 21.04.2021.
//

import Foundation

struct ChatMessageModel {
    let text: String
    let imgUrl: String
    let date: Date
}
