//
//  StartScreenPresenter.swift
//
//  Created Azapsh on 23.04.2021.

import Foundation

// MARK: View
protocol StartScreenViewProtocol: AnyObject {

}

// MARK: Presenter
protocol StartScreenPresenterProtocol: AnyObject {
    func viewDidLoad()
}

class StartScreenPresenter: StartScreenPresenterProtocol {

    weak private var view: StartScreenViewProtocol?
    private var router: StartScreenRouterProtocol

    init(view: StartScreenViewProtocol, router: StartScreenRouterProtocol) {
        self.router = router
        self.view = view
    }
    
    func viewDidLoad() {
        
    }
}
