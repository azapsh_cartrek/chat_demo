//
//  ChatPresenter.swift
//
//  Created Ахмед Фокичев on 21.04.2021.

import Foundation

// MARK: View
protocol ChatViewProtocol: AnyObject {
    func tableReload()
    func addMessage()
}

// MARK: Presenter
protocol ChatPresenterProtocol: AnyObject {
    var messages: [ChatMessageModel] { get }
    func viewDidLoad()
    func sendMassege(chatMessageModel: ChatMessageModel)
}

class ChatPresenter: ChatPresenterProtocol {

    var messages = [ChatMessageModel]()
    
    weak private var view: ChatViewProtocol?
    private var router: ChatRouterProtocol

    init(view: ChatViewProtocol, router: ChatRouterProtocol) {
        self.router = router
        self.view = view
    }
    
    func viewDidLoad() {
        messages.append(ChatMessageModel(text: "434fdfg fgfhgh ggfhgh ghg j hj gggggggggg gfhgh ghg j hj gggggggggg gfhgh ghg j hj gggggggggg gggggg gggggg ggggggg ", imgUrl: "", date: Date() ))
        messages.append(ChatMessageModel(text: "434fdfg fgfhgh ghg j hj hjh j", imgUrl: "", date: Date() ))
        messages.append(ChatMessageModel(text: "434fdfg fgfhgh ghg j hj gggggggggggghjh j", imgUrl: "", date: Date() ))
        messages.append(ChatMessageModel(text: "434fdfg fgfhgh ghg j hj hjh 434fdfg fgfhgh ghg j hj hjhj", imgUrl: "", date: Date() ))
        messages.append(ChatMessageModel(text: "434fdfg fgfhgh ghg j hj hjh j", imgUrl: "", date: Date() ))
        messages.append(ChatMessageModel(text: "434fdfg fgfgggggggg4fdfg fgfhgh g ggggggggggghg j hj hgggggg ggggggg gggggggg gggggg gggggggjhj", imgUrl: "", date: Date() ))
        messages.append(ChatMessageModel(text: "434fdfg fgfhgh ghg j hj hjh j", imgUrl: "", date: Date() ))
        messages.append(ChatMessageModel(text: "434fdfg f", imgUrl: "", date: Date() ))
        view?.tableReload()
    }
    
    func sendMassege(chatMessageModel: ChatMessageModel) {
        messages.append(chatMessageModel)
        view?.addMessage()
    }
}
