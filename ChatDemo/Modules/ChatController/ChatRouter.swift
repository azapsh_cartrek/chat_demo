//
//  ChatRouter.swift
//
//  Created Ахмед Фокичев on 21.04.2021.

import UIKit

// MARK: Router
protocol ChatRouterProtocol: AnyObject {

}

class ChatRouter: ChatRouterProtocol {

    weak var view: UIViewController?

    init (view: UIViewController) {
        self.view = view
    }
}
