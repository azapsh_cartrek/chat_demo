//
//  StartScreenRouter.swift
//
//  Created Azapsh on 23.04.2021.

import UIKit

// MARK: Router
protocol StartScreenRouterProtocol: AnyObject {

}

class StartScreenRouter: StartScreenRouterProtocol {

    weak var view: UIViewController?

    init (view: UIViewController) {
        self.view = view
    }
}
