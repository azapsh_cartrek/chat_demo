//
//  NavigationBarView.swift
//  ChatDemo
//
//  Created by Azapsh on 24.04.2021.
//

import UIKit
import SnapKit

protocol NavigationBarViewDelegate: AnyObject {
    func backTapped(tag: Int)
}

extension NavigationBarViewDelegate {
    func backTapped(tag: Int) {}
}

class NavigationBarView: UIView {

    // MARK: Constants
    private enum Constants {
        static let width: CGFloat = 46
    }
    
    // MARK: SubViews
    private let backButton: UIButton = {
        let button = UIButton()
        button.setTitle("back", for: .normal)
        button.backgroundColor = .gray
        return button
    }()
    
    private let titleInfo: UILabel = {
        let label = UILabel()
        label.text = "34343 rtr5r tyhtyghgh ghhhh ghhhhgh gh  ggggg"
        label.numberOfLines = 0
        label.textColor = .black
        label.backgroundColor = .yellow
        return label
    }()
    
    private let calendarButton: UIButton = {
        let button = UIButton()
        button.setTitle("caldr", for: .normal)
        button.backgroundColor = .red
        return button
    }()
    
    private let notificationButton: UIButton = {
        let button = UIButton()
        button.setTitle("notif", for: .normal)
        button.backgroundColor = .red
        return button
    }()
    
    private let shareButton: UIButton = {
        let button = UIButton()
        button.setTitle("share", for: .normal)
        button.backgroundColor = .red
        return button
    }()
    
    private let menuButton: UIButton = {
        let button = UIButton()
        button.setTitle("menu", for: .normal)
        button.backgroundColor = .red
        return button
    }()

    
    private let stackView: UIStackView = {
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .horizontal
        stack.spacing = 2
        
        return stack
    }()
    
    var delegate: NavigationBarViewDelegate?
    
    // MARK: LifeCycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func loadView() {
        self.addSubview(stackView)
        stackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        stackView.addArrangedSubview(titleInfo)
        titleInfo.snp.makeConstraints {
            $0.height.equalToSuperview()
            $0.centerY.equalToSuperview()
        }
    }
    
    func setTitle(text: String) {
        titleInfo.text = text
    }
    
    func setTitleAlign(align: NSTextAlignment) {
        titleInfo.textAlignment = align
    }
    
    func addButton(icon: UIImage, tapIcon: UIImage? = nil, tag: Int, at: Int? = nil) {
        let button = UIButton()
        button.tag = tag
        button.setImage(icon, for: .normal)
        button.addTarget(self, action: #selector(buttonTapped(_:)), for: .touchUpInside)
        
        if let tapIcon = tapIcon {
            button.setImage(tapIcon, for: .highlighted)
        }
        if let at = at {
            stackView.insertArrangedSubview(button, at: at)
        } else {
            stackView.addArrangedSubview(button)
        }
        
        button.snp.makeConstraints {
            $0.width.equalTo(Constants.width)
        }
    }
    
    @objc func buttonTapped(_ sender: UIButton) {
        delegate?.backTapped(tag: sender.tag)
    }
}
