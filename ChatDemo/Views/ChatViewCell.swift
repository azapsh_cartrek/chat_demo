//
//  ChatViewCell.swift
//  ChatDemo
//
//  Created by Ахмед Фокичев on 21.04.2021.
//

import UIKit
import SnapKit

protocol ChatViewCellProtocol {
    func configure(chatMessageModel: ChatMessageModel)
}

class ChatViewCell: UITableViewCell, ChatViewCellProtocol {

    static let identifier: String = "cell_identifier"

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        loadView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: SubViews
    private let label: UILabel = {
        let label = UILabel()
        label.backgroundColor = .orange
        label.textColor = .black
        label.numberOfLines = 0
        return label
    }()
    
    var chatMessageModel: ChatMessageModel?

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    private func loadView() {
        self.addSubview(label)
        label.snp.makeConstraints {
            $0.width.equalTo(200)
            $0.leading.equalToSuperview()
            $0.bottom.top.equalToSuperview().inset(4)
        }
    }
    
    func configure(chatMessageModel: ChatMessageModel) {
        print("ChatViewCell>", chatMessageModel.text)
        label.text = chatMessageModel.text
    }
}
