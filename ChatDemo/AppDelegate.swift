//
//  AppDelegate.swift
//  ChatDemo
//
//  Created by Ахмед Фокичев on 21.04.2021.
//

import UIKit
import IQKeyboardManagerSwift

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 100
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate, let window = appDelegate.window  {
            let viewController = ChatViewController()
            let router =  ChatRouter(view: viewController)
            let presenter = ChatPresenter(view: viewController, router: router)
            viewController.setup(presenter: presenter)
            
            let navigationController = UINavigationController(rootViewController: viewController)
            navigationController.setNavigationBarHidden(false, animated: false)
            window.rootViewController = navigationController
            window.makeKeyAndVisible()
        }
        
        return true
    }

    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
        return true
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Registration(google firebase) failed!")
    }

}

